# Vizualizace RGB LED svetel v 3D prostoru

## Jak spustit vybuildenou verzi

```
cd dist/client/
npx serve . (nebo jakykoliv jiny webserver)
```

## Návod

Klikněte do scény a pohybujte se prostorem.
Stisknutim ESC se vrati kurzor a muzete pouzivat \GUI prvky

## Veřejná verze

https://kovarlukas-pgrf2.netlify.app

## Co v budoucnu

-   přidat více scén
-   zvážit přechod na react-three-fiber, který staví na threejs na react konceptech které lze dost dobře škálovat. A má realtime devflow
