const PORT_NUMBER = 8;
const PIXEL_COUNT = 144;
const FPS = 30;

var portBuffers = [];

var initilized = false;

var instance = 0; // interface instance

const HEAPU32_PTR = function (ptr) {
  return ptr / 4;
};

function getColorString(r, g, b) {
  return "#" + ("0" + r.toString(16)).slice(-2) + ("0" + g.toString(16)).slice(-2) + ("0" + b.toString(16)).slice(-2);
}

function toUint8Array(numbers) {
  const arrayBuffer = new ArrayBuffer(numbers.length);
  const uint8Array = new Uint8Array(arrayBuffer);
  for (let i = 0; i < numbers.length; i++) {
    uint8Array[i] = numbers[i];
  }
  return uint8Array;
}

function hexStringToNumberArray(hexString) {
  var numberArray = [];
  for (var i = 0; i < hexString.length; i += 2) {
    var hexPair = hexString.substr(i, 2);
    var number = parseInt(hexPair, 16);
    numberArray.push(number);
  }
  return numberArray;
}

function makeInstance() {
  const label_string = "con1";
  const label_bytes_size = label_string.length;
  const label_bytes_ptr = Module._malloc(label_bytes_size + 1); // Allocate memory for the array

  for (let i = 0; i < label_bytes_size; i++) {
    Module.HEAPU8[label_bytes_ptr + i] = label_string[i].charCodeAt(0);
  }
  Module.HEAPU8[label_bytes_ptr + label_bytes_size] = 0; // NULL terminator

  instance = Module._makeInstance(label_bytes_ptr);

  Module._free(label_bytes_ptr);
}

function makePorts() {
  if (!initilized) {
    return;
  }

  Module._setClock(instance, 0);

  for (let i = 0; i < PORT_NUMBER; i++) {
    let portChar = String.fromCharCode("A".charCodeAt(0) + i);

    const portBufferPtr = Module._makePort(instance, portChar.charCodeAt(0), PIXEL_COUNT, 255, 255, true, false);
    portBuffers.push(new Uint8Array(Module.HEAPU8.buffer, portBufferPtr, PIXEL_COUNT * 3));
  }
}

function setClock() {
  if (!initilized) {
    return;
  }

  const clock_timestamp = document.getElementById("clock_timestamp").value;

  Module._setClock(instance, clock_timestamp);
}

function execute() {
  if (!initilized) {
    return;
  }

  const command_hexstring = document.getElementById("execute_bytecode").value;
  const command_bytes = toUint8Array(hexStringToNumberArray(command_hexstring));

  const command_bytes_size = command_bytes.length;
  const command_bytes_ptr = Module._malloc(command_bytes_size); // Allocate memory for the array
  Module.HEAPU8.set(command_bytes, command_bytes_ptr); // Copy the array data into the WASM memory

  Module._execute(instance, command_bytes_ptr, command_bytes_size, 0xffff);

  Module._free(command_bytes_ptr); // Free the memory when you're done with it
}

function request() {
  if (!initilized) {
    return;
  }

  const request_hexstring = document.getElementById("execute_bytecode").value;
  const request_bytes = toUint8Array(hexStringToNumberArray(request_hexstring));

  const request_bytes_size = request_bytes.length;
  const request_bytes_ptr = Module._malloc(request_bytes_size); // Allocate memory for the array
  Module.HEAPU8.set(request_bytes, request_bytes_ptr); // Copy the array data into the WASM memory

  const response_result_ptr = Module._malloc(12);

  // INTERFACE RequestResult request(const uint8_t* const request_bytecode, const size_t request_bytecode_size, const connection_handle_t source_connection)
  const success = Module._request(instance, request_bytes_ptr, request_bytes_size, response_result_ptr);

  const response_bytecode_ptr = Module.HEAPU32[HEAPU32_PTR(response_result_ptr)];
  const response_bytecode_size = Module.HEAPU32[HEAPU32_PTR(response_result_ptr) + 1];
  const request_evaluate_result = Module.HEAPU32[HEAPU32_PTR(response_result_ptr) + 2];

  const response_bytes = HEAPU8.subarray(response_bytecode_ptr, response_bytecode_ptr + response_bytecode_size);

  Module._free(request_bytes_ptr);
  Module._free(response_bytecode_ptr);
  Module._free(response_result_ptr);
}

const renderer_dom = document.querySelector("#render");

function show() {
  if (!initilized) {
    return;
  }

  Module._render(instance);

  renderer_dom.innerHTML = "";

  for (var i = 0; i < PORT_NUMBER; i++) {
    // Create a div to hold the column of cubes
    var column = document.createElement("div");
    column.className = "column";

    const title = document.createElement("span");
    title.className = "column-title";
    title.innerHTML = `port ${String.fromCharCode("A".charCodeAt(0) + i)}`;
    renderer_dom.appendChild(column);

    var columnContent = document.createElement("div");
    columnContent.className = "column-content";
    column.appendChild(columnContent);

    for (var j = PIXEL_COUNT - 1; j >= 0; j--) {
      var cube = document.createElement("div");
      cube.className = "cube";
      cube.style.backgroundColor = getColorString(portBuffers[i][j * 3 + 0], portBuffers[i][j * 3 + 1], portBuffers[i][j * 3 + 2]);
      columnContent.appendChild(cube);
    }
    column.appendChild(title);
  }
}

window.addEventListener("message", event => {
  console.log("Received message from parent:", event.data);
  const data = JSON.parse(event.data);

  if (data.shouldReload) {
    location.reload();
  } else if (data.js_eval) {
    eval(data.js_eval);
  } else if (data.execute_bytecode) {
    document.querySelector("#execute_bytecode").value = data.execute_bytecode;
    execute();
  } else if (data.request_bytecode) {
    document.querySelector("#request_bytecode").value = data.request_bytecode;
    request();
  } else if (data.clock_timestamp) {
    document.querySelector("#clock_timestamp").value = data.clock_timestamp;
    setClock();
  }
});
