const logging = {
  error: console.error,
  warn: console.warn,
  info: logging.debug,
  debug: logging.debug,
  verbose: logging.debug,
};

class SpectodaSimulation {
  /** @type {TextEncoder} */
  #encoder;

  constructor() {
    this.#encoder = new TextEncoder();
  }

  // allocates C string from JS text. When done call free()
  #mallocCharPtr(text) {
    const string_bytes = this.#encoder.encode(text);

    // Allocate memory for the string in the WASM memory
    const char_ptr = Module._malloc(string_bytes.byteLength + 1); // +1 for the null terminator

    Module.HEAPU8.set(string_bytes, char_ptr);
    Module.HEAPU8.set(0, char_ptr + string_bytes.byteLength);

    return char_ptr;
  }

  // returns label in "label" string or null
  #processLabel(text) {

    if(typeof(text) !== "string") {
        return null;
    }

    let regex = /\$?([\w]+)/.exec(label);

    if (!regex) {
        logging.error("Invalid label", text);
        return null;
    }

    return regex[1];
    }

    // returns percentage in -100.0 to 100.0 float number or null
    #processPercentage(value) {

        if(!(typeof(value) === "string" || typeof(value) === "number")) {
            return null;
        }

        if (typeof(value) === "string") {
            let regex = /([+-]?[\d]*\.?[\d]*)%/.exec(value);

            if(!regex || parseFloat(regex[1]) === NaN) {
                logging.error("Invalid percentage", value);
                return null;
            }

            value = parseFloat(regex[1]);
        }

        if(value > 100.0) {
            logging.info("Capping percentage to 100.0%");
            return 100.0;
        }

        if(value < -100.0) {
            logging.info("Capping percentage to -100.0%");
            return -100.0;
        }

       return value;


    }

    // returns color in "#ff00aa" string or null
    #processColor(text) {

        if(typeof(text) !== "string") {
            return null;
        }

        // TODO add color strings line "red", "blue" and so on

        let regex = /#?([0-9a-f]{6})/i.exec(label);

        if (!regex) {
            logging.error("Invalid color", text);
            return null;
        }

        return "#" + regex[1];
    }

     // returns timestamp in miliseconds as number or null
     #processTimestamp(value) {

        if(!(typeof(value) === "string" || typeof(value) === "number")) {
            logging.error("Invalid timestamp", value);
            return null;
        }

        if (typeof(value) === "string") {

            // first test for Infinity

            if (/-Infinity/.test(value)) {
                return -2147483648; // INT32 min
            }

            if (/+?Infinity/.test(value)) {
                return 2147483647; // INT32 max
            }
  
            let days = value.match(/([+-]?[0-9]+[.]?[0-9]*|[.][0-9]+)\s*d/gi);
            let hours = value.match(/([+-]?[0-9]+[.]?[0-9]*|[.][0-9]+)\s*h/gi);
            let minutes = value.match(/([+-]?[0-9]+[.]?[0-9]*|[.][0-9]+)\s*m(?!s)/gi);
            let secs = value.match(/([+-]?[0-9]+[.]?[0-9]*|[.][0-9]+)\s*s/gi);
            let msecs = value.match(/([+-]?[0-9]+[.]?[0-9]*|[.][0-9]+)\s*(t|ms)/gi);

            if(!days && !hours && !minutes && !secs && !msecs) {
                logging.error("invalid timestamp", value);
                return null;
            }
        
            // logging.debug(days);
            // logging.debug(hours);
            // logging.debug(minutes);
            // logging.debug(secs);
            // logging.debug(msecs);
        
            value = 0;
        
            while (days && days.length) {
              let d = parseFloat(days[0]);
              value += d * 86400000;
              days.shift();
            }
        
            while (hours && hours.length) {
              let h = parseFloat(hours[0]);
              value += h * 3600000;
              hours.shift();
            }
        
            while (minutes && minutes.length) {
              let m = parseFloat(minutes[0]);
              value += m * 60000;
              minutes.shift();
            }
        
            while (secs && secs.length) {
              let s = parseFloat(secs[0]);
              value += s * 1000;
              secs.shift();
            }
        
            while (msecs && msecs.length) {
              let ms = parseFloat(msecs[0]);
              value += ms;
              msecs.shift();
            }
        }

        if (value > 2147483647) {
            logging.info("Capping timestamp to 2147483647 ms");
            return 2147483647;
        } 

        if (value < -2147483648) {
            logging.info("Capping timestamp to -2147483648 ms");
            return -2147483648;
        } 

        return value;

    }

    // returns destination as number or null
    #processDestination(number) {

        if(typeof(number) !== "number" || number > 255 || number < 0) {
            logging.error("Invalid destination", number)
            return null;
        }

        return number;
    }

    emitEmptyEvent(label, destination=255) {
        logging.debug("emitEmptyEvent", label, destination);

        if (Array.isArray(destination)) {
          for (const dst in destination) {
            this.emitEmptyEvent(label, dst);
          }
        }

        label = this.#processLabel(label);
        destination = this.#processDestination(label);

        if (label === null || destination === null) {
          return Promise.reject("InvalidParameter");
        }

        const labelMemPtr = this.#mallocCharPtr(label);
        Module._emitEmptyEvent(labelMemPtrm, destination);
        Module._free(labelMemPtr);
    }

    emitPercentageEvent(label, value, destination=255) {
        logging.debug("emitPercentageEvent", label, value, destination);

        if (Array.isArray(destination)) {
          for (const dst in destination) {
            this.emitPercentageEvent(label, value, dst);
          }
        }

        label = this.#processLabel(label);
        value = this.#processPercentage(value);
        destination = this.#processDestination(label);

        if (label === null || value === null || destination === null) {
          return Promise.reject("InvalidParameter");
        }

        const labelMemPtr = this.#mallocCharPtr(label);
        Module._emitPercentageEvent(labelMemPtrm, value, destination);
        Module._free(labelMemPtr);
    }

    emitColorEvent(label, value, destination=255) {
      logging.debug("emitColorEvent", label, value, destination);

      if (Array.isArray(destination)) {
        for (const dst in destination) {
          this.emitColorEvent(label, value, dst);
        }
      }

      label = this.#processLabel(label);
      value = this.#processColor(value);
      destination = this.#processDestination(label);

      if (label === null || value === null || destination === null) {
        return Promise.reject("InvalidParameter");
      }

      const [r, g, b] = hexColorToRGB(value);

      const labelMemPtr = this.#mallocCharPtr(label);
      Module._emitColorEvent(labelMemPtrm, r, g, b, destination);
      Module._free(labelMemPtr);
    }

    emitTimestampEvent(label, value, destination=255) {
        logging.debug("emitTimestampEvent", label, value, destination);

        if (Array.isArray(destination)) {
          for (const dst in destination) {
            this.emitTimestampEvent(label, value, dst);
          }
        }

        label = this.#processLabel(label);
        value = this.#processTimestamp(value);
        destination = this.#processDestination(label);

        if (label === null || value === null || destination === null) {
          return Promise.reject("InvalidParameter");
        }

        const labelMemPtr = this.#mallocCharPtr(label);
        Module._emitTimestampEvent(labelMemPtrm, value, destination);
        Module._free(labelMemPtr);
    }

    emitLabelEvent(label, value, destination=255) {
        logging.debug("emitLabelEvent", label, value, destination);

        if (Array.isArray(destination)) {
          for (const dst in destination) {
            this.emitLabelEvent(label, value, dst);
          }
        }

        label = this.#processLabel(label);
        value = this.#processLabel(value);
        destination = this.#processDestination(label);

        if (label === null || value === null || destination === null) {
          return Promise.reject("InvalidParameter");
        }

        const labelMemPtr = this.#mallocCharPtr(label);
        const valueMemPtr = this.#mallocCharPtr(value);
        Module._emitLabelEvent(labelMemPtrm, valueMemPtr, destination);
        Module._free(labelMemPtr);
        Module._free(valueMemPtr);
    }

  emitEvent(label, value=null, destination=255) {
    logging.debug("emitEvent", label, value, destination);

    if(Array.isArray(destination)) {
        for (const dst in destination) {
            this.emitEvent(label, value, dst);            
        }
    }

    label = this.#processLabel(label);
    destination = this.#processDestination(label);

    if (label === null || destination === null) {
      return Promise.reject("InvalidParameter");
    }

    const labelMemPtr = this.#mallocCharPtr(label);

    let temp;

    if (value === null || value === undefined) {
      logging.debug("Module._emitEmptyEvent", label, destination);
      Module._emitEmptyEvent(labelMemPtr, destination);
    } 
    
    else if ((temp = this.#processColor(value))) {
      logging.debug("Module._emitColorEvent", label, temp, destination);
      Module._emitColorEvent(labelMemPtr, temp, destination);
    } 
    
    else if ((temp = this.#processTimestamp(value))) {
      logging.debug("Module._emitTimestampEvent", label, temp, destination);
      Module._emitTimestampEvent(labelMemPtr, temp, destination);
    } 
    
    else if ((temp = this.#processLabel(value))) {
      logging.debug("Module._emitLabelEvent", label, temp, destination);
      const valueMemPtr = this.#mallocCharPtr(temp);
      Module._emitLabelEvent(labelMemPtr, valueMemPtr, destination);
      Module._free(valueMemPtr);
    } 
    
    else if ((temp = this.#processPercentage(value))) {
      logging.debug("Module._emitPercentageEvent", label, temp, destination);
      Module._emitPercentageEvent(labelMemPtr, temp, destination);
    }

    Module._free(labelMemPtr);
  }
}
