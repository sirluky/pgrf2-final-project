import { Color, Mesh } from 'three'
import { portBuffers } from './defs'

export function getRandomHexColor() {
    const hexValues = '0123456789ABCDEF'
    let color = '#'
    for (let i = 0; i < 6; i++) {
        color += hexValues[Math.floor(Math.random() * 16)]
    }
    return color
}

export function resizeRendererToDisplaySize(renderer: any) {
    const canvas = renderer.domElement
    const width = canvas.clientWidth
    const height = canvas.clientHeight
    const needResize = canvas.width !== width || canvas.height !== height
    if (needResize) {
        renderer.setSize(width, height, false)
    }
    return needResize
}

// Create a canvas to draw the LED strip colors
const canvas = document.createElement('canvas')
const ctx = canvas.getContext('2d')

export function generateColorsAndCanvas() {
    let ledColors = []

    // fill it with 100 random colors
    for (let i = 0; i < 100; i++) {
        ledColors.push(Math.floor(Math.random() * 0xffffff))
    }
    ctx.clearRect(0, 0, canvas.width, canvas.height)

    // Set the canvas size according to the number of LEDs in the strip
    canvas.width = ledColors.length
    canvas.height = 1

    // Draw the LED colors on the canvas
    ledColors.forEach((color, index) => {
        ctx.fillStyle = `#${color.toString(16)}`
        ctx.fillRect(index, 0, 1, 1)
    })

    return { ledColors, canvas }
}

export function setSimulationColorsOnStrip() {
    const ledColors = getColorsFromBuffer(0, 100)

    // fill it with 100 random colors

    ctx.clearRect(0, 0, canvas.width, canvas.height)

    // Set the canvas size according to the number of LEDs in the strip
    canvas.width = ledColors.length
    canvas.height = 1

    // Draw the LED colors on the canvas
    ledColors.forEach((color, index) => {
        ctx.fillStyle = color.getStyle()
        ctx.fillRect(index, 0, 1, 1)
    })

    return { ledColors, canvas }
}

export function getColorsFromBuffer(portNumber: number, length: number) {
    let colors = []
    for (let i = 0; i < length; i++) {
        colors.push(
            new Color(
                portBuffers[portNumber][i * 3 + 0] / 255,
                portBuffers[portNumber][i * 3 + 1] / 255,
                portBuffers[portNumber][i * 3 + 2] / 255
            )
        )

        // let integerColor =
        //     (portBuffers[portNumber][i * 3 + 0] << 16) +
        //     (portBuffers[portNumber][i * 3 + 1] << 8) +
        //     portBuffers[portNumber][i * 3 + 2]
        // colors.push(integerColor)
    }
    return colors
}
