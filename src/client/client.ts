// @ts-nocheck

import * as THREE from 'three'
import {
    BoxGeometry,
    Color,
    CylinderGeometry,
    Group,
    Mesh,
    RingGeometry,
    TubeGeometry,
} from 'three'
import { PointerLockControls } from 'three/examples/jsm/controls/PointerLockControls'

import {
    generateColorsAndCanvas,
    getRandomHexColor,
    resizeRendererToDisplaySize,
    setSimulationColorsOnStrip,
} from './utils'
import Stats from 'stats.js'
import {
    PIXEL_COUNT,
    execute,
    getColorString,
    initilized,
    instance,
    makeInstance,
    makePorts,
    portBuffers,
    request,
    setClock,
    setInitilized,
    show,
} from './defs'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader'
import { GUI } from 'dat.gui'
import { gui, initGui } from './gui'
import { LedStrip } from './ledstrip'
import './lightShowDemo'
import { onRuntimeInitialized } from './lightShowDemo'

let initialized = false
let benchmarkEnabled = true

let isSimulationMode = false

export function setSimulationMode(simulationMode: boolean) {
    isSimulationMode = simulationMode
}

export function disableBenchmark() {
    benchmarkEnabled = false
}

export let scene: THREE.Scene
export let camera: THREE.Camera

function main() {
    const renderer = new THREE.WebGLRenderer({ antialias: true })
    renderer.setSize(window.innerWidth, window.innerHeight)
    renderer.setPixelRatio(window.devicePixelRatio)

    document.body.appendChild(renderer.domElement)

    let camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000)
    camera.position.z = 2
    camera.position.set(122.85325309857927, 126.09826240650982, -363.1151873454021)
    camera.rotation.set(-2.0503797254960805, 0.6376680346039026, 2.288839483373995)

    camera.position.set(39.745838638332884, 30.468939454733878, -87.1014561722553)

    camera.rotation.set(-2.94935523391615, 0.26311135651780115, 3.091012412046747)

    // @ts-ignore
    window.camera = camera
    const controls = new PointerLockControls(camera, renderer.domElement)

    scene = new THREE.Scene()
    const geometry = new THREE.BoxGeometry(1, 1, 1)

    const material = new THREE.MeshPhongMaterial({
        color: 'pink',
    })

    const light = new THREE.DirectionalLight(0xffffff, 1)
    light.position.set(10, 10, 10)
    light.castShadow = true
    light.shadow.mapSize.width = 2048
    light.shadow.mapSize.height = 2048

    const axesHelper = new THREE.AxesHelper(5)
    scene.add(axesHelper)
    const meshes: Group[] = []

    function randomizeMeshes(mesh: Group) {
        for (let i = 0; i < mesh.children.length; i++) {
            // @ts-ignore
            const m = (mesh.children[i].material.color = new Color(
                Math.random(),
                Math.random(),
                Math.random()
            ))
        }
    }

    // function updateLedStrip(newPixelAmount: number) {
    //     // Remove the old LED strip from the scene
    //     for (let mesh of meshes) {
    //         scene.remove(mesh)
    //     }

    //     // Clear the meshes array
    //     meshes.length = 0

    //     // Create a new LED strip with the updated amount of pixels
    //     for (let i = 0; i < PIXEL_COUNT; i++) {
    //         const { ledStrip, ledStripLight } = generateRgbLedStrip(
    //             newPixelAmount,
    //             1,
    //             -i * 3,
    //             1,
    //             0.2,
    //             0xff0000
    //         )
    //         scene.add(ledStrip)
    //         meshes.push(ledStrip)

    //         scene.add(ledStripLight)
    //     }
    // }

    const keys = {
        w: false,
        a: false,
        s: false,
        d: false,
    }

    window.addEventListener('keydown', onKeyDown, false)
    window.addEventListener('keyup', onKeyUp, false)

    function onKeyDown(event: KeyboardEvent) {
        switch (event.code) {
            case 'KeyW':
                keys.w = true
                break
            case 'KeyA':
                keys.a = true
                break
            case 'KeyS':
                keys.s = true
                break
            case 'KeyD':
                keys.d = true
                break
        }
    }

    function onKeyUp(event: KeyboardEvent) {
        switch (event.code) {
            case 'KeyW':
                keys.w = false
                break
            case 'KeyA':
                keys.a = false
                break
            case 'KeyS':
                keys.s = false
                break
            case 'KeyD':
                keys.d = false
                break
        }
    }
    const cameraSpeed = 1

    function moveCamera() {
        if (keys.w) {
            camera.position.add(
                camera.getWorldDirection(new THREE.Vector3()).multiplyScalar(cameraSpeed)
            )
        }
        if (keys.s) {
            camera.position.sub(
                camera.getWorldDirection(new THREE.Vector3()).multiplyScalar(cameraSpeed)
            )
        }
        if (keys.a) {
            const left = new THREE.Vector3().crossVectors(
                camera.up,
                camera.getWorldDirection(new THREE.Vector3())
            )
            camera.position.add(left.multiplyScalar(cameraSpeed))
        }
        if (keys.d) {
            const right = new THREE.Vector3().crossVectors(
                camera.getWorldDirection(new THREE.Vector3()),
                camera.up
            )
            camera.position.add(right.multiplyScalar(cameraSpeed))
        }
    }

    // updateLedStrip(8)

    renderer.domElement.addEventListener('click', () => {
        controls.lock()
    })

    const stats = new Stats()
    stats.showPanel(0) // 0: fps, 1: ms, 2: mb, 3+: custom
    document.body.appendChild(stats.dom)

    let loadedModel: GLTF
    const glftLoader = new GLTFLoader()
    glftLoader.load('./assets/shiba/scene.gltf', (gltfScene) => {
        loadedModel = gltfScene
        // console.log(loadedModel);

        gltfScene.scene.rotation.y = Math.PI / 8
        gltfScene.scene.position.y = 14
        gltfScene.scene.scale.set(10, 10, 10)
        scene.add(gltfScene.scene)
    })

    glftLoader.load('./assets/cube/Cube.gltf', (gltfScene) => {
        loadedModel = gltfScene

        gltfScene.scene.position.x = 30
        gltfScene.scene.position.y = 20
        // console.log(loadedModel);

        gltfScene.scene.rotation.y = Math.PI / 8
        gltfScene.scene.scale.set(10, 10, 10)
        gltfScene.scene.castShadow = true
        gltfScene.scene.receiveShadow = false
        scene.add(gltfScene.scene)

        // const light = new THREE.DirectionalLight(0xffffff)
        const light = new THREE.RectAreaLight(0xffffff, 0.9, 100, 100)

        light.position.x = 50
        light.position.y = 40
        light.lookAt(0, 0, 10)

        // const helper = new THREE.DirectionalLightHelper(light, 5)
        // scene.add(helper)
        scene.add(light)

        // const rectLightHelper = new THREE.RectAreaLightHelper(light)
        // light.add(rectLightHelper)
        // const light = new THREE.PointLight(0xffffff, 1, 100)

        // light.setRotationFromAxisAngle(new THREE.Vector3(1, 2, 0), Math.PI / 4)
        // scene.add(light)
    })
    // ledStripLight.position.set(150.7268668644509, 5.854663673931949, 35.16859613502015)
    // mesh.position.set(150.7268668644509, 10.854663673931949, 35.16859613502015)
    let strip = new LedStrip(
        new THREE.Vector3(150.7268668644509, 0.854663673931949, -30.16859613502015),
        140,

        new Color(0xff0000)
    )
    window.ledStrip = strip

    scene.add(strip.getGroup())

    const pointLight = new THREE.PointLight(0xffffff, 0.8, 300)
    pointLight.position.set(0, 0, 0) // Set the light position relative to the camera
    camera.add(pointLight)
    scene.add(camera)

    // camera.position.set(53.7268668644509, 17.854663673931949, -454.16859613502015)
    camera.position.set(-13.940462538541805, 50.458125554732618, -52.94202507821071)
    camera.rotation.set(-2.9900887519690817, -1.0773912579823424, -3.007930675838034)

    const spotLight = new THREE.SpotLight(0xffffff, 1, 1000, 0.5, 1)
    spotLight.castShadow = true
    spotLight.position.set(53.7268668644509, 30.854663673931949, -454.16859613502015)
    scene.add(spotLight)

    const otherGui = gui.addFolder('Ostatní')
    otherGui.open()

    pointLight.visible = false
    otherGui.add(pointLight, 'visible').name('Osvětlení kamery')

    glftLoader.load('./assets/sponza/Sponza.gltf', (gltfScene) => {
        // loadedModel = gltfScene
        // console.log(loadedModel);

        // gltfScene.scene.rotation.y = Math.PI / 8
        // gltfScene.scene.position.y = 3
        gltfScene.scene.scale.set(30, 30, 30)
        scene.add(gltfScene.scene)
    })

    function render(time: number) {
        if (loadedModel) {
            loadedModel.scene.rotation.y += 0.01
        }

        if (!initilized) {
            requestAnimationFrame(render)
            return
        }

        Module._render(instance)

        stats.begin()
        moveCamera()

        // for (let i = 0; i < meshes.length; i++) {
        //     const meshGroup = meshes[i]
        //     let y = i % PIXEL_COUNT
        //     let x = i
        //     if (benchmarkEnabled) {
        //         randomizeMeshes(meshGroup)
        //     } else {
        //         setMeshColor(x, meshGroup)
        //     }
        // }
        strip.showSimulation()

        if (isSimulationMode) {
            window.updateStrip()
        }

        if (resizeRendererToDisplaySize(renderer)) {
            const canvas = renderer.domElement
            camera.aspect = canvas.clientWidth / canvas.clientHeight
            camera.updateProjectionMatrix()
        }

        time *= 0.003 // convert time to seconds

        renderer.render(scene, camera)

        requestAnimationFrame(render)
        stats.end()
    }

    // camera.position.set(53.7268668644509, 3, -30.16859613502015)

    initGui(camera)
    const ambientLight = new THREE.AmbientLight(0xffffff, 0.2)

    // setup directional light + helper
    // const dl = new THREE.DirectionalLight(0xffffff, 0.5)
    // use this for YouTube thumbnail
    // dl.position.set(0, 2, 2)
    // dl.position.set(0, 2, 0)
    // dl.castShadow = true
    // const dlHelper = new THREE.DirectionalLightHelper(dl, 3, new Color('#ff0000'))
    // scene.add(dlHelper)
    // scene.add(dl)

    scene.add(ambientLight)

    const hugeBottom = new THREE.Mesh(
        new THREE.BoxGeometry(1000, 0.1, 1000),
        new THREE.MeshStandardMaterial({ color: 0xffffff })
    )
    hugeBottom.position.y = -1
    scene.add(hugeBottom)

    generateLightStrips()
    requestAnimationFrame(render)
}

function generateLightStrips() {
    let spacing = 1

    const { canvas, ledColors } = generateColorsAndCanvas()

    // Create a texture from the canvas
    const texture = new THREE.CanvasTexture(canvas)

    // Create a RectAreaLight with the LED strip texture
    const ledStripWidth = ledColors.length * spacing // Adjust spacing as needed
    const ledStripHeight = 1

    // Function to create a point light for each LED color
    function createLedPointLight(color, position) {
        const light = new THREE.PointLight(color, 4, 50)
        light.position.set(position.x, position.y, position.z)
        return light
    }

    // Create point lights for each LED and add them to the scene

    const ledStripLight = new THREE.Group()
    for (let i = 0; i <= ledColors.length; i += 10) {
        const color = ledColors[i]
        const position = new THREE.Vector3(i * spacing - ledStripWidth / 2 + 2, 0, 0)
        const pointLight = createLedPointLight(color, position)
        ledStripLight.add(pointLight)
    }

    // Create a mesh to display the LED strip texture
    const geometry = new THREE.PlaneGeometry(ledStripWidth, ledStripHeight)
    const material = new THREE.MeshBasicMaterial({ map: texture, side: THREE.DoubleSide })
    const mesh = new THREE.Mesh(geometry, material)

    // Position the mesh and the light
    ledStripLight.position.set(150.7268668644509, 5.854663673931949, 35.16859613502015)
    mesh.position.set(150.7268668644509, 10.854663673931949, 35.16859613502015)

    function nahodneBarvy() {
        const { canvas, ledColors } = generateColorsAndCanvas()

        // Update the texture with the new canvas image
        texture.needsUpdate = true

        // Update the point light colors
        for (let i = 0; i < ledColors.length; i += 10) {
            if (!ledStripLight.children[i]) {
                continue
            }
            const pointLight = ledStripLight.children[i].color.set(ledColors[i])
        }
    }
    gui.add({ nahodneBarvy }, 'nahodneBarvy')

    function updateStrip() {
        const { canvas, ledColors } = setSimulationColorsOnStrip()

        // if (texture.needsUpdate !== true) {
        //     // Update the texture with the new canvas image
        texture.needsUpdate = true
        // }

        // Update the point light colors
        for (let i = 0; i < ledColors.length; i += 10) {
            if (!ledStripLight.children[Math.floor(i / 10)]) {
                continue
            }
            ledStripLight.children[Math.floor(i / 10)].color.set(ledColors[i])
        }
    }
    window.updateStrip = updateStrip

    // Add the light and the mesh to the scene
    scene.add(ledStripLight)
    scene.add(mesh)
}

function injectScript(src: string) {
    return new Promise((resolve, reject) => {
        if (typeof window !== 'undefined' && document) {
            const script = document.createElement('script')
            script.src = src
            script.addEventListener('load', resolve)
            script.addEventListener('error', (e) => reject(e.error))
            document.head.appendChild(script)
        }
    })
}

const wasm_js_file = 'DEBUG_0.9.0_20230302.js'
if (typeof window !== 'undefined') {
    // First try to load local version
    injectScript(`${wasm_js_file}`)
        .then(onWasmLoad)
        .catch((error) => {
            console.error(error)
            // if local version fails, load public file
            injectScript(
                `https://updates.spectoda.com/subdom/updates/webassembly/daily/${wasm_js_file}`
            )
                .then(onWasmLoad)
                .catch((error) => {
                    console.error(error)
                })
        })
}

function onWasmLoad() {
    // @ts-ignore
    Module.onRuntimeInitialized = () => {
        setInitilized(true)

        window.Module = Module
        console.log('WASM initialized', Module)
        main()

        makeInstance()
        makePorts()
        setClock()
        execute()
        request()
        show()
        onRuntimeInitialized()
    }
}
