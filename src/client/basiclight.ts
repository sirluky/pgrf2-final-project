// @ts-nocheck

import * as THREE from 'three'
import {
    BoxGeometry,
    Color,
    CylinderGeometry,
    Group,
    Mesh,
    RingGeometry,
    TubeGeometry,
} from 'three'
import { PointerLockControls } from 'three/examples/jsm/controls/PointerLockControls'

import { getRandomHexColor, resizeRendererToDisplaySize } from './utils'
import Stats from 'stats.js'
import {
    PIXEL_COUNT,
    execute,
    getColorString,
    initilized,
    instance,
    makeInstance,
    makePorts,
    portBuffers,
    request,
    setClock,
    setInitilized,
    show,
} from './defs'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader'
import { GUI } from 'dat.gui'

let initialized = false
let benchmarkEnabled = true

export function disableBenchmark() {
    benchmarkEnabled = false
}

function generateRgbLedStrip(
    x: number,
    y: number,
    z: number,
    length: number,
    spacing: number,
    color: number
) {
    // Create a new group to hold the cubes
    const ledStrip = new THREE.Group()

    // Calculate the total length of the LED strip
    const totalLength = (x - 1) * spacing + x * length

    // Create a loop to generate the cubes
    for (let i = 0; i < x; i++) {
        // Calculate the position of the current cube
        const posX = i * (length + spacing) * 5
        const posY = 0
        const posZ = z

        // Create a new cube with the specified dimensions and position
        const cubeGeometry = new THREE.BoxGeometry(length, y, y)
        const cubeMaterial = new THREE.MeshBasicMaterial({ color })
        const cube = new THREE.Mesh(cubeGeometry, cubeMaterial)

        // cube.castShadow = true
        // cube.receiveShadow = true
        const ambientLight = new THREE.AmbientLight(0xffffff, 0.4)
        cube.add(ambientLight)
        cube.position.set(posX, posY, posZ)
        // Add the cube to the LED strip group
        ledStrip.add(cube)
    }

    // Position the LED strip group so that it's centered on the origin
    ledStrip.position.x = -totalLength / 2

    // Return the LED strip group
    return ledStrip
}

function main() {
    const renderer = new THREE.WebGLRenderer({ antialias: true })
    renderer.setSize(window.innerWidth, window.innerHeight)
    renderer.setPixelRatio(window.devicePixelRatio)

    document.body.appendChild(renderer.domElement)

    const camera = new THREE.PerspectiveCamera(
        75,
        window.innerWidth / window.innerHeight,
        0.1,
        1000
    )
    camera.position.z = 2
    camera.position.set(122.85325309857927, 126.09826240650982, -363.1151873454021)
    camera.rotation.set(-2.0503797254960805, 0.6376680346039026, 2.288839483373995)
    // @ts-ignore
    window.camera = camera
    const controls = new PointerLockControls(camera, renderer.domElement)

    const scene = new THREE.Scene()
    const geometry = new THREE.BoxGeometry(1, 1, 1)

    const material = new THREE.MeshPhongMaterial({
        color: 'pink',
    })

    let objects: Mesh[] = []

    const light = new THREE.DirectionalLight(0xffffff, 1)
    light.position.set(10, 10, 10)
    light.castShadow = true
    light.shadow.mapSize.width = 2048
    light.shadow.mapSize.height = 2048

    const axesHelper = new THREE.AxesHelper(5)
    scene.add(axesHelper)
    const meshes: Group[] = []

    function randomizeMeshes(mesh: Group) {
        for (let i = 0; i < mesh.children.length; i++) {
            // @ts-ignore
            const m = (mesh.children[i].material.color = new Color(
                Math.random(),
                Math.random(),
                Math.random()
            ))
        }
    }

    function setMeshColor(y: number, mesh: Mesh) {
        for (let x = 0; x < mesh.children.length; x++) {
            mesh.children[x].material.color = new Color(
                portBuffers[x][y * 3 + 0] / 255,
                portBuffers[x][y * 3 + 1] / 255,
                portBuffers[x][y * 3 + 2] / 255
            )
        }
    }

    function updateLedStrip(newPixelAmount: number) {
        // Remove the old LED strip from the scene
        for (let mesh of meshes) {
            scene.remove(mesh)
        }

        // Clear the meshes array
        meshes.length = 0

        // Create a new LED strip with the updated amount of pixels
        for (let i = 0; i < PIXEL_COUNT; i++) {
            const mesh = generateRgbLedStrip(newPixelAmount, 1, -i * 3, 1, 0.2, 0xff0000)
            scene.add(mesh)
            meshes.push(mesh)
        }
    }

    const keys = {
        w: false,
        a: false,
        s: false,
        d: false,
    }

    window.addEventListener('keydown', onKeyDown, false)
    window.addEventListener('keyup', onKeyUp, false)

    function onKeyDown(event: KeyboardEvent) {
        switch (event.code) {
            case 'KeyW':
                keys.w = true
                break
            case 'KeyA':
                keys.a = true
                break
            case 'KeyS':
                keys.s = true
                break
            case 'KeyD':
                keys.d = true
                break
        }
    }

    function onKeyUp(event: KeyboardEvent) {
        switch (event.code) {
            case 'KeyW':
                keys.w = false
                break
            case 'KeyA':
                keys.a = false
                break
            case 'KeyS':
                keys.s = false
                break
            case 'KeyD':
                keys.d = false
                break
        }
    }
    const cameraSpeed = 1

    function moveCamera() {
        if (keys.w) {
            camera.position.add(
                camera.getWorldDirection(new THREE.Vector3()).multiplyScalar(cameraSpeed)
            )
        }
        if (keys.s) {
            camera.position.sub(
                camera.getWorldDirection(new THREE.Vector3()).multiplyScalar(cameraSpeed)
            )
        }
        if (keys.a) {
            const left = new THREE.Vector3().crossVectors(
                camera.up,
                camera.getWorldDirection(new THREE.Vector3())
            )
            camera.position.add(left.multiplyScalar(cameraSpeed))
        }
        if (keys.d) {
            const right = new THREE.Vector3().crossVectors(
                camera.getWorldDirection(new THREE.Vector3()),
                camera.up
            )
            camera.position.add(right.multiplyScalar(cameraSpeed))
        }
    }

    function createSlider() {
        const sliderContainer = document.createElement('div')
        sliderContainer.style.position = 'fixed'
        sliderContainer.style.top = '10px'
        sliderContainer.style.left = '10px'
        sliderContainer.style.zIndex = '100'

        const label = document.createElement('label')
        label.setAttribute('for', 'pixelSlider')
        label.textContent = 'Number of pixels: '

        const slider = document.createElement('input')
        slider.setAttribute('type', 'range')
        slider.setAttribute('id', 'pixelSlider')
        slider.setAttribute('min', '8')
        slider.setAttribute('max', '100')
        slider.setAttribute('value', '8')

        sliderContainer.appendChild(label)
        sliderContainer.appendChild(slider)
        document.body.appendChild(sliderContainer)

        return slider
    }

    // const pixelSlider = createSlider()

    // pixelSlider.addEventListener('change', onSliderChange)
    updateLedStrip(8)

    function onSliderChange(event: any) {
        const newPixelAmount = parseInt(event.target.value)
        updateLedStrip(newPixelAmount)
    }

    renderer.domElement.addEventListener('click', () => {
        controls.lock()
    })

    const stats = new Stats()
    stats.showPanel(0) // 0: fps, 1: ms, 2: mb, 3+: custom
    document.body.appendChild(stats.dom)

    let loadedModel: GLTF
    const glftLoader = new GLTFLoader()
    glftLoader.load('./assets/shiba/scene.gltf', (gltfScene) => {
        loadedModel = gltfScene
        // console.log(loadedModel);

        gltfScene.scene.rotation.y = Math.PI / 8
        gltfScene.scene.position.y = 3
        gltfScene.scene.scale.set(10, 10, 10)
        scene.add(gltfScene.scene)
    })

    // ledStripFolder.add(ledStrip.position, 'x', -1000, 1000, 0.01)
    // ledStripFolder.add(ledStrip.position, 'y', -1000, 1000, 0.01)
    // ledStripFolder.add(ledStrip.position, 'z', -1000, 1000, 0.01)

    function render(time: number) {
        if (loadedModel) {
            loadedModel.scene.rotation.y += 0.01
        }

        if (!initilized) {
            requestAnimationFrame(render)
            return
        }

        Module._render(instance)

        stats.begin()
        moveCamera()

        for (let i = 0; i < meshes.length; i++) {
            const meshGroup = meshes[i]
            let y = i % PIXEL_COUNT
            let x = i
            if (benchmarkEnabled) {
                randomizeMeshes(meshGroup)
            } else {
                setMeshColor(x, meshGroup)
            }
        }

        if (resizeRendererToDisplaySize(renderer)) {
            const canvas = renderer.domElement
            camera.aspect = canvas.clientWidth / canvas.clientHeight
            camera.updateProjectionMatrix()
        }

        time *= 0.003 // convert time to seconds

        for (let o of objects) {
            o.rotation.x = time
            o.rotation.y = time / 1.2
        }

        renderer.render(scene, camera)

        requestAnimationFrame(render)
        stats.end()
    }

    requestAnimationFrame(render)
}

function injectScript(src: string) {
    return new Promise((resolve, reject) => {
        if (typeof window !== 'undefined' && document) {
            const script = document.createElement('script')
            script.src = src
            script.addEventListener('load', resolve)
            script.addEventListener('error', (e) => reject(e.error))
            document.head.appendChild(script)
        }
    })
}

const wasm_js_file = 'DEBUG_0.9.0_20230302.js'
if (typeof window !== 'undefined') {
    // First try to load local version
    injectScript(`${wasm_js_file}`)
        .then(onWasmLoad)
        .catch((error) => {
            console.error(error)
            // if local version fails, load public file
            injectScript(
                `https://updates.spectoda.com/subdom/updates/webassembly/daily/${wasm_js_file}`
            )
                .then(onWasmLoad)
                .catch((error) => {
                    console.error(error)
                })
        })
}

function onWasmLoad() {
    // @ts-ignore
    Module.onRuntimeInitialized = () => {
        setInitilized(true)

        window.Module = Module
        console.log('WASM initialized', Module)
        main()

        makeInstance()
        makePorts()
        setClock()
        execute()
        request()
        show()
    }
}
