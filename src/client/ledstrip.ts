// @ts-nocheck
import * as THREE from 'three'
import { scene } from './client'
import { getColorsFromBuffer } from './utils'

export class LedStrip {
    private ports: any[] = []
    private portsCount: number = 8
    private length: number = 0
    private color: THREE.Color
    private group: THREE.Group = new THREE.Group()

    private position: THREE.Vector3 = new THREE.Vector3()

    constructor(position: THREE.Vector3, length: number, color: THREE.Color) {
        this.length = length
        this.color = color
        this.position = position

        for (let i = 0; i < this.portsCount; i++) {
            let strip = this.createStrip(this.length, this.color)
            this.ports.push(strip)
        }

        this.group = new THREE.Group()
        this.group.position.copy(this.position)

        for (let i = 0; i < this.ports.length; i++) {
            let strip = this.ports[i]
            strip.position.z = i * 1
            this.group.add(strip)
        }

        this.group.scale.set(5, 5, 5)

        // scene.add(group)
    }

    public getGroup() {
        return this.group
    }

    private createStrip(length: number, color: THREE.Color) {
        const lightEmitGroup = new THREE.Group()
        const strip = new THREE.Group()

        for (let i = 0; i < length; i++) {
            let led = new THREE.Mesh(
                new THREE.BoxGeometry(0.1, 0.1, 0.1),
                new THREE.MeshBasicMaterial({
                    color: color,
                })
            )
            led.position.x = i * 0.2
            strip.add(led)
        }
        // add light to every tenth led
        for (let i = 0; i < length; i++) {
            if (i % 10 === 0) {
                let light = new THREE.PointLight(color, 1, 10)
                light.position.x = i * 0.2
            }
        }

        return strip
    }

    showRandomLed() {
        for (let i = 0; i < this.ports.length; i++) {
            let strip = this.ports[i]
            for (let j = 0; j < strip.children.length; j++) {
                let led = strip.children[j] as THREE.Mesh
                led.material.color.setRGB(Math.random(), Math.random(), Math.random())
            }
        }
    }

    showSimulation() {
        for (let i = 0; i < this.ports.length; i++) {
            getColorsFromBuffer(i, this.length).forEach((color, index) => {
                let led = this.ports[i].children[index] as THREE.Mesh
                led.material.color = color
            })
        }
    }
}
