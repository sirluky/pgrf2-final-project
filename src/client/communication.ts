import { setLoggingLevel } from '../lib/spectoda-js/Logging'
import { Spectoda } from '../lib/spectoda-js/Spectoda'
// todo refactor, remove
const primaryColor = '#ff8c5c'

const spectodaDevice = new Spectoda('default', 0)
// console.log("AutoConnect")
// Matty key
if (typeof window !== 'undefined') {
    spectodaDevice.assignOwnerSignature(
        localStorage.getItem('ownerSignature') || 'a06cd5c4d5741b61fee69422f2590926'
    )
    spectodaDevice.assignOwnerKey(
        localStorage.getItem('ownerKey') || 'bfd39c89ccc2869f240508e9a0609420'
    )

    // @ts-ignore
    window.spectodaDevice = spectodaDevice
    process.env.NODE_ENV === 'development' && setLoggingLevel(4)

    const url = new URL(location.href)
    const params = new URLSearchParams(url.search)

    setTimeout(async () => {
        await spectodaDevice.assignConnector('dummy')
        spectodaDevice.connect()
    }, 300)
}

export { spectodaDevice }
