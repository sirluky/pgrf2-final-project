import { setSimulationMode } from './client'
import { spectodaDevice } from './communication'
import { gui } from './gui'

const tnglOfShow = `defDevice($con1, 0x00, 0xff, 144px, $A1, 0x00, 144px, $B1, 0x00, 144px, $C1, 0x00, 144px, $D1, 0x00, 144px, $E1, 0x00, 144px, $F1, 0x00, 144px, $G1, 0x00, 144px, $H1, 0x00);

defCanvas($cvs1, {
  segment($A1);
  segment($B1);
  segment($C1);
  segment($D1);
  segment($E1);
  segment($F1);
  segment($G1);
  segment($H1);
});

defCanvas($bot, {
  segment($A1bot);
  segment($B1bot);
  segment($C1bot);
  segment($D1bot);
  segment($E1bot);
  segment($F1bot);
  segment($G1bot);
  segment($H1bot);
});

defSegment($A1bot, 0x00, { slice($A1, 0px, 60px, 1px); });

defSegment($B1bot, 0x00, { slice($B1, 0px, 60px, 1px); });

defSegment($A1top, 0x00, { slice($A1, 94px, 60px, 1px); });

defSegment($C1bot, 0x00, { slice($C1, 0px, 60px, 1px); });

defSegment($B1top, 0x00, { slice($B1, 94px, 60px, 1px); });

defSegment($D1bot, 0x00, { slice($D1, 0px, 60px, 1px); });

defSegment($C1top, 0x00, { slice($C1, 94px, 60px, 1px); });

defSegment($E1bot, 0x00, { slice($E1, 0px, 60px, 1px); });

defSegment($D1top, 0x00, { slice($D1, 94px, 60px, 1px); });

defSegment($F1bot, 0x00, { slice($F1, 0px, 60px, 1px); });

defSegment($E1top, 0x00, { slice($E1, 94px, 60px, 1px); });

defSegment($G1bot, 0x00, { slice($G1, 0px, 60px, 1px); });

defSegment($F1top, 0x00, { slice($F1, 94px, 60px, 1px); });

defCanvas($top, {
  segment($A1top);
  segment($B1top);
  segment($C1top);
  segment($D1top);
  segment($E1top);
  segment($F1top);
  segment($G1top);
  segment($H1top);
});

defSegment($H1bot, 0x00, { slice($H1, 0px, 60px, 1px); });

defSegment($G1top, 0x00, { slice($G1, 94px, 60px, 1px); });

defSegment($H1top, 0x00, { slice($H1, 94px, 60px, 1px); });

// Loading visualization
  addLayer(0s, 9000ms, {
    siftCanvases({ canvas($cvs1, $__RAN, 10s); }, {
      addDrawing(-1s, 10500ms, animPlasmaShot(0.3s, #ffffff, 25%));
    });
  }).modifyFadeIn(10s);
  addLayer(0s, 9000ms, {
    siftCanvases({ canvas($cvs1, $__RAN, 10s); }, {
      addDrawing(0s, 9500ms, animPlasmaShot(0.3s, #1c1c1c, 25%));
    });
  });
  addLayer(2s, 7600ms, {
    siftSegments({ segment($D1); }, {
      addDrawing(0s, 10000ms, animLoadingBar(7s, #ffffff, #000000).animFill(Infinity, #ffffff));
    });
  }).modifyFadeIn(2s);
  addLayer(3s, 6600ms, {
    siftSegments({ segment($A1); }, {
      addDrawing(0s, 10000ms, animLoadingBar(6s, #ffffff, #000000).animFill(Infinity, #ffffff));
    });
  }).modifyFadeIn(2s);
  addLayer(4s, 5600ms, {
    siftSegments({ segment($B1); }, {
      addDrawing(0s, 10000ms, animLoadingBar(5s, #ffffff, #000000).animFill(Infinity, #ffffff));
    });
  }).modifyFadeIn(2s);
  addLayer(5s, 4600ms, {
    siftSegments({ segment($F1); }, {
      addDrawing(0s, 10000ms, animLoadingBar(4s, #ffffff, #000000).animFill(Infinity, #ffffff));
    });
  }).modifyFadeIn(2s);
  addLayer(6s, 3600ms, {
    siftSegments({ segment($H1); }, {
      addDrawing(0s, 10000ms, animLoadingBar(3s, #ffffff, #000000).animFill(Infinity, #ffffff));
    });
  }).modifyFadeIn(2s);
  addLayer(6s, 3600ms, {
    siftSegments({ segment($G1); }, {
      addDrawing(0s, 10000ms, animLoadingBar(3s, #ffffff, #000000).animFill(Infinity, #ffffff));
    });
  }).modifyFadeIn(2s);
  addLayer(7s, 2600ms, {
    siftSegments({ segment($C1); }, {
      addDrawing(0s, 10000ms, animLoadingBar(2s, #ffffff, #000000).animFill(Infinity, #ffffff));
    });
  }).modifyFadeIn(2s);
  addLayer(7s, 2600ms, {
    siftSegments({ segment($E1); }, {
      addDrawing(0s, 10000ms, animLoadingBar(2s, #ffffff, #000000).animFill(Infinity, #ffffff));
    });
  }).modifyFadeIn(2s);

// Snakes & Segment sliding part
  const colorSwitchDelay = 0.58s;
  addLayer(8.35s, 20450ms, {
    siftCanvases({ canvas($cvs1, $__R2L, 0.2s); }, {
      addDrawing(1.25s, 15350ms, animFill(&colorSwitchDelay, #242424).animFill(&colorSwitchDelay, #800000).animFill(&colorSwitchDelay, #008700).animFill(&colorSwitchDelay, #000087));
    });
  });
  addLayer(15s, 13800ms, {
    siftCanvases({ canvas($bot, $__R2L, 0.2s); }, {
      addDrawing(0s, 16600ms, animFill(&colorSwitchDelay, #242424).animFill(&colorSwitchDelay, #ffff00).animFill(&colorSwitchDelay, #00ffff).animFill(&colorSwitchDelay, #360036));
    });
  });
  addLayer(15s, 13800ms, {
    siftCanvases({ canvas($top, $__L2R, 0.2s); }, {
      addDrawing(0s, 16600ms, animFill(&colorSwitchDelay, #242424).animFill(&colorSwitchDelay, #800000).animFill(&colorSwitchDelay, #008700).animFill(&colorSwitchDelay, #000087));
    });
  });
  addLayer(17.2s, 12800ms, {
    siftSegments({ segment($H1); }, {
      addDrawing(0s, 1000ms, animPlasmaShot(1s, #ffffff, 30%));
    });
    siftSegments({ segment($G1); }, {
      addDrawing(1.2s, 1000ms, animPlasmaShot(-1s, #ffffff, 30%));
    });
    siftSegments({ segment($F1); }, {
      addDrawing(2.4s, 1000ms, animPlasmaShot(1s, #ffffff, 30%));
    });
    siftSegments({ segment($E1); }, {
      addDrawing(3.6s, 1000ms, animPlasmaShot(-1s, #ffffff, 30%));
    });
    siftSegments({ segment($D1); }, {
      addDrawing(4.8s, 1000ms, animPlasmaShot(1s, #ffffff, 30%));
    });
    siftSegments({ segment($C1); }, {
      addDrawing(6s, 1000ms, animPlasmaShot(-1s, #ffffff, 30%));
    });
    siftSegments({ segment($B1); }, {
      addDrawing(7.2s, 1000ms, animPlasmaShot(1s, #ffffff, 30%));
    });
    siftSegments({ segment($A1); }, {
      addDrawing(8.4s, 1000ms, animPlasmaShot(-1s, #ffffff, 30%));
    });
  });
  addLayer(17.2s, 12800ms, {
    siftSegments({ segment($H1); }, {
      addDrawing(0s, 1000ms, animPlasmaShot(1s, #ffffff, 30%));
    });
    siftSegments({ segment($G1); }, {
      addDrawing(1.2s, 1000ms, animPlasmaShot(-1s, #ffffff, 30%));
    });
    siftSegments({ segment($F1); }, {
      addDrawing(2.4s, 1000ms, animPlasmaShot(1s, #ffffff, 30%));
    });
    siftSegments({ segment($E1); }, {
      addDrawing(3.6s, 1000ms, animPlasmaShot(-1s, #ffffff, 30%));
    });
    siftSegments({ segment($D1); }, {
      addDrawing(4.8s, 1000ms, animPlasmaShot(1s, #ffffff, 30%));
    });
    siftSegments({ segment($C1); }, {
      addDrawing(6s, 1000ms, animPlasmaShot(-1s, #ffffff, 30%));
    });
    siftSegments({ segment($B1); }, {
      addDrawing(7.2s, 1000ms, animPlasmaShot(1s, #ffffff, 30%));
    });
    siftSegments({ segment($A1); }, {
      addDrawing(8.4s, 1000ms, animPlasmaShot(-1s, #ffffff, 30%));
    });
  });
  addLayer(19s, 11000ms, {
    siftSegments({ segment($A1); }, {
      addDrawing(0s, 1000ms, animPlasmaShot(1s, #ffffff, 30%));
    });
    siftSegments({ segment($B1); }, {
      addDrawing(1.2s, 1000ms, animPlasmaShot(-1s, #ffffff, 30%));
    });
    siftSegments({ segment($C1); }, {
      addDrawing(2.4s, 1000ms, animPlasmaShot(1s, #ffffff, 30%));
    });
    siftSegments({ segment($D1); }, {
      addDrawing(3.6s, 1000ms, animPlasmaShot(-1s, #ffffff, 30%));
    });
    siftSegments({ segment($E1); }, {
      addDrawing(4.8s, 1000ms, animPlasmaShot(1s, #ffffff, 30%));
    });
    siftSegments({ segment($F1); }, {
      addDrawing(6s, 1000ms, animPlasmaShot(-1s, #ffffff, 30%));
    });
    siftSegments({ segment($G1); }, {
      addDrawing(7.2s, 1000ms, animPlasmaShot(1s, #ffffff, 30%));
    });
    siftSegments({ segment($H1); }, {
      addDrawing(8.4s, 1000ms, animPlasmaShot(-1s, #ffffff, 30%));
    });
  });
  addLayer(16s, 14000ms, {
    siftSegments({ segment($A1); }, {
      addDrawing(0s, 1000ms, animPlasmaShot(1s, #ffffff, 30%));
    });
    siftSegments({ segment($B1); }, {
      addDrawing(1.2s, 1000ms, animPlasmaShot(-1s, #ffffff, 30%));
    });
    siftSegments({ segment($C1); }, {
      addDrawing(2.4s, 1000ms, animPlasmaShot(1s, #ffffff, 30%));
    });
    siftSegments({ segment($D1); }, {
      addDrawing(3.6s, 1000ms, animPlasmaShot(-1s, #ffffff, 30%));
    });
    siftSegments({ segment($E1); }, {
      addDrawing(4.8s, 1000ms, animPlasmaShot(1s, #ffffff, 30%));
    });
    siftSegments({ segment($F1); }, {
      addDrawing(6s, 1000ms, animPlasmaShot(-1s, #ffffff, 30%));
    });
    siftSegments({ segment($G1); }, {
      addDrawing(7.2s, 1000ms, animPlasmaShot(1s, #ffffff, 30%));
    });
    siftSegments({ segment($H1); }, {
      addDrawing(8.4s, 1000ms, animPlasmaShot(-1s, #ffffff, 30%));
    });
  });
  addLayer(15s, 14000ms, {
    siftSegments({ segment($A1); }, {
      addDrawing(0s, 1000ms, animPlasmaShot(1s, #ffffff, 30%));
    });
    siftSegments({ segment($B1); }, {
      addDrawing(1.2s, 1000ms, animPlasmaShot(-1s, #ffffff, 30%));
    });
    siftSegments({ segment($C1); }, {
      addDrawing(2.4s, 1000ms, animPlasmaShot(1s, #ffffff, 30%));
    });
    siftSegments({ segment($D1); }, {
      addDrawing(3.6s, 1000ms, animPlasmaShot(-1s, #ffffff, 30%));
    });
    siftSegments({ segment($E1); }, {
      addDrawing(4.8s, 1000ms, animPlasmaShot(1s, #ffffff, 30%));
    });
    siftSegments({ segment($F1); }, {
      addDrawing(6s, 1000ms, animPlasmaShot(-1s, #ffffff, 30%));
    });
    siftSegments({ segment($G1); }, {
      addDrawing(7.2s, 1000ms, animPlasmaShot(1s, #ffffff, 30%));
    });
    siftSegments({ segment($H1); }, {
      addDrawing(8.4s, 1000ms, animPlasmaShot(-1s, #ffffff, 30%));
    });
  });

// Sines
  addLayer(28.8s, 21200ms, {
    siftCanvases({ canvas($cvs1, $__L2R, 1s); }, {
      addDrawing(0s, 20000ms, animLoadingBar(0.8s, #ffffff, #000000).animLoadingBar(-0.8s, #ffffff, #000000));
      addDrawing(9.9s, 10100ms, animPlasmaShot(0.5s, #0000ff, 25%));
      addLayer(19s, Infinity, {
        addDrawing(0s, Infinity, animFill(Infinity, #ffffff));
      }).modifyFadeIn(1s);
    });
  });
  addLayer(33s, 17000ms, {
    siftCanvases({ canvas($cvs1, $__L2R, 1.8s); }, {
      addDrawing(0s, 20000ms, animLoadingBar(2s, #303030, #000000).animLoadingBar(-2s, #303030, #000000));
      addDrawing(9.9s, 10100ms, animPlasmaShot(0.5s, #0000ff, 25%));
      addLayer(19s, Infinity, {
        addDrawing(0s, Infinity, animFill(Infinity, #ffffff));
      }).modifyFadeIn(1s);
    });
  });
  addLayer(35s, 15000ms, {
    siftCanvases({ canvas($cvs1, $__L2R, 1s); }, {
      addDrawing(0s, 20000ms, animLoadingBar(1s, #212121, #000000).animLoadingBar(-1s, #212121, #000000));
    });
  });

siftSegments({ segment($A1top); segment($B1top); segment($C1top); segment($D1top); segment($E1top); segment($F1top); segment($G1top); segment($H1top); }, {
  addDrawing(50.44s, 660ms, animFill(Infinity, #ffffff));
});
siftSegments({ segment($A1bot); segment($B1bot); segment($C1bot); segment($D1bot); segment($E1bot); segment($F1bot); segment($G1bot); segment($H1bot); }, {
  addDrawing(50.8s, 300ms, animFill(Infinity, #ffffff));
});
siftSegments({ segment($A1); segment($B1); segment($C1); segment($D1); segment($E1); segment($F1); segment($G1); segment($H1); }, {
  addDrawing(51.046s, 54ms, animFill(Infinity, #ffffff));
});
siftCanvases({ canvas($cvs1, $__L2R, 0s); }, {
  addDrawing(51.1s, 700ms, animFill(Infinity, #ffffff));
  addLayer(51.8s, 4700ms, {
    addDrawing(0s, Infinity, animLoadingBar(-4.7s, #ffffff, #000000));
  });
});
siftCanvases({ canvas($cvs1, $__L2R, 0.1s); }, {
  addDrawing(52.8s, 500ms, animPlasmaShot(0.5s, #00ff00, 25%));
  addDrawing(55.3s, 500ms, animPlasmaShot(0.5s, #00ff00, 25%));
});
siftCanvases({ canvas($cvs1, $__STR, 0s); }, {
  addDrawing(57s, 1100ms, animFill(0.1s, #ffffff).animFade(1s, #ffffff, #000000));
});
`

const mp3 = document.createElement('audio')
mp3.src = 'audio.mp3'
mp3.controls = true

// container.innerHTML = 'Loading audio and Simulation...'
const container = document.createElement('div')
container.style.position = 'fixed'
container.style.bottom = '0px'
container.style.right = '0px'
container.style.margin = '10px'
container.style.padding = '10px'
container.style.backgroundColor = 'transparent'
container.style.border = '1px solid black'
document.body.appendChild(mp3)
container.appendChild(mp3)
document.body.appendChild(container)

mp3.onplay = () => {
    writeTnglAndPlay()
    spectodaDevice.timeline.unpause()
    spectodaDevice.timeline.setMillis()
    spectodaDevice.syncTimeline()

    setSimulationMode(true)
}

export function onRuntimeInitialized() {
    // container.innerHTML = ''
}

function writeTnglAndPlay() {
    spectodaDevice.writeTngl(tnglOfShow)
}
// gui.add()
